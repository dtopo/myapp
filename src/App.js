import React, {Component} from 'react';

import Particles from 'react-particles-js';
 
import Clarifai from 'clarifai';


import './App.css';


import Navigation from './components/Navigation/Navigation';

import Logo from './components/Logo/Logo';

import ImageLinkForm from './components/ImageLinkForm/ImageLinkForm';
import FaceRecognition from './components/FaceRecognition/FaceRecognition';

import Rank from './components/Rank/Rank';


const app = new Clarifai.App({
  apiKey: '8dec03147dfb4900aac93e9ab431348c'
});





const particlesOption = {
  
    particles: {


      "number": {
        "value": 275,
        "density": {
          "enable": true,
          "value_area": 1025.8919341219544
        }
      },
      "color": {
        "value": "#0a0a5c"
      },
      "shape": {
        "type": "triangle",
        "stroke": {
          "width": 4,
          "color": "#e8129c"
        },
        "polygon": {
          "nb_sides": 4
        },
        "image": {
          "src": "img/github.svg",
          "width": 100,
          "height": 100
        }
      },
      "opacity": {
        "value": 0.4419226793140727,
        "random": false,
        "anim": {
          "enable": false,
          "speed": 0.9744926547616151,
          "opacity_min": 0.1,
          "sync": false
        }
      },
      "size": {
        "value": 3.5,
        "random": true,
        "anim": {
          "enable": false,
          "speed": 273.32667332667336,
          "size_min": 77.52247752247752,
          "sync": false
        }
      },
      "line_linked": {
        "enable": true,
        "distance": 111,
        "color": "#390b3c",
        "opacity": 0.06413648243462085,
        "width": 11.363726039504726
      },
      "move": {
        "enable": true,
        "speed": 3,
        "direction": "top-right",
        "random": false,
        "straight": false,
        "out_mode": "out",
        "bounce": false,
        "attract": {
          "enable": false,
          "rotateX": 320.68241217310424,
          "rotateY": 1200
        }
      }
    },
    "interactivity": {
      "detect_on": "canvas",
      "events": {
        "onhover": {
          "enable": true,
          "mode": "grab"
        },
        "onclick": {
          "enable": false,
          "mode": "push"
        },
        "resize": true
      },
      "modes": {
        "grab": {
          "distance": 400,
          "line_linked": {
            "opacity": 1
          }
        },
        "bubble": {
          "distance": 400,
          "size": 40,
          "duration": 2,
          "opacity": 8,
          "speed": 3
        },
        "repulse": {
          "distance": 200,
          "duration": 0.4
        },
        "push": {
          "particles_nb": 4
        },
        "remove": {
          "particles_nb": 2
        }
      }
    },
    "retina_detect": true
        /* number:{
          value: 111,
          density:{
            enable: true,
            value_area: 800
          }
          
        } */
      /* line_linked: {
        shadow: {
          enable: true,
          color: "#3CA9D1",
          blur: 5
        }
      } */
    
}


class App extends Component{

  constructor(){
    super();
    this.state = {
      input : '',
      imageUrl: '',
    }
  }

  onInputChange = (event) => {
    this.setState({input: event.target.value});
  }

  onButtonSubmit = () => {
    this.setState({imageUrl: this.state.input});

        app.models.predict( Clarifai.FACE_DETECT_MODEL,
          this.state.input).then(
          function(response) {
            console.log(response.outputs[0].data.regions[0].region_info.bounding_box);
          },
          function(err){

          }
        );
  }




  render(){
    return (
        <div className="App">

              <Particles className='particles'
                params={particlesOption}
              />

          <Navigation />
          <Logo />
          <Rank />
          <ImageLinkForm  onInputChange={this.onInputChange} 
                          onButtonSubmit={this.onButtonSubmit} />
          
          <FaceRecognition imageUrl={this.state.imageUrl}/>
        </div>
  );
}
}

export default App;
  